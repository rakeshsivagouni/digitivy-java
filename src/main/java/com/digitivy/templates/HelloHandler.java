package com.digitivy.templates;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class HelloHandler implements RequestHandler<String, String> {
    @Override
    public String handleRequest(String input, Context context) {
        context.getLogger().log("Input: " + input);

        //TODO: Implement your handler
        String output = "Hello, " + input + "!";
        return output;
    }
}
